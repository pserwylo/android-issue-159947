# Reproduction of Android issue 159947

This is a minimal project which shows the bug described in [Issue 159947: Stacked Notifications don't show up on pre-Lollipop devices - setGroup.](https://code.google.com/p/android/issues/detail?id=159947).

This works with expected behaviour in:
 * <= Gingerbread
 * >= Lollipop

It fails to show notifications with `setGroup()` when on KitKat.
