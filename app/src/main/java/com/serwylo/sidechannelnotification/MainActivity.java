package com.serwylo.sidechannelnotification;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import sidechannelnotification.serwylo.com.sidechannelnotification.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.button_with_group).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notifyWithGroup();
            }
        });

        findViewById(R.id.button_without_group).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notifyWithoutGroup();
            }
        });
    }

    private void notifyWithGroup() {
        PendingIntent intent = PendingIntent.getActivity(this, 0, new Intent(), 0);

        Notification notification = new android.support.v4.app.NotificationCompat.Builder(this)
                .setAutoCancel(true)
                .setContentTitle("With Group")
                .setContentIntent(intent)
                .setSmallIcon(android.R.drawable.ic_menu_add)
                .setGroup("Group")
                .build();

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(1, notification);
    }

    private void notifyWithoutGroup() {
        PendingIntent intent = PendingIntent.getActivity(this, 0, new Intent(), 0);

        Notification notification = new android.support.v4.app.NotificationCompat.Builder(this)
                .setAutoCancel(true)
                .setContentTitle("Without Group")
                .setContentIntent(intent)
                .setSmallIcon(android.R.drawable.ic_menu_add)
                .build();

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(2, notification);
    }
}
